# WineVault

 My test to mobile development company [WoxApp] (http://woxapp.com/). 

### Note:

 1. app correctly views on tablets, such as nexus 9 (screen 8.9")
 
 2. in cause of failed sign in, just enter "q" in login without password, and sign in success.

###  References

1. download [APK from Github] (https://github.com/SergeyBurlaka/WineVault/blob/master/wine_vault_v-2.apk) 

2. test [my app working on-line ] (https://appetize.io/app/719truq5739yceg3zfht7p04br?device=nexus9&scale=75&orientation=landscape&osVersion=7.0) 

3. quickly and easily move to [.../src/main/] (https://github.com/SergeyBurlaka/WineVault/tree/master/app/src/main)


### Screenshots

 "wine_vault_v-2"

<img src="https://github.com/SergeyBurlaka/WineVault/blob/master/img/WineVault_v4_login.jpg" > 

<img src="https://github.com/SergeyBurlaka/WineVault/blob/master/img/WineVault_v4.jpg">


 "wine_vault_v-1"

<img src="https://github.com/SergeyBurlaka/WineVault/blob/master/img/2016-10-24%2014-40-04%20Screenshot_2.jpg" >


